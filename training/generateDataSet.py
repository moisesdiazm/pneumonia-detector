import os, shutil

original_dataset_dir = 'chest_dataset'
base_dir = 'pneumonia_dataset'
os.mkdir(base_dir)

train_dir = os.path.join(base_dir, 'train')
os.mkdir(train_dir)
validation_dir = os.path.join(base_dir, 'validation')
os.mkdir(validation_dir)
test_dir = os.path.join(base_dir, 'test')
os.mkdir(test_dir)

test_pneuomonia_dir = os.path.join(test_dir, 'pneumonia')
os.mkdir(test_pneuomonia_dir)
test_normal_dir = os.path.join(test_dir, 'normal')
os.mkdir(test_normal_dir)

fnames = ['pneumonia.{}.jpg'.format(i) for i in range(1341 + 1)]
for fname in fnames:
    src = os.path.join(original_dataset_dir, fname)
    dst = os.path.join(test_pneuomonia_dir, fname)
    shutil.copyfile(src, dst)
