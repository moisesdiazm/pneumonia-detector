import os
import numpy as np
from PIL import Image

from skimage import data, img_as_float, io
from skimage import exposure, color

directories = ['test', 'train', 'val']
subdirectories = ['NORMAL', 'PNEUMONIA']

# os.mkdir('dataPreprocessed')  

for directory in directories:
    try:
        os.mkdir('dataPreprocessed/' + directory)
    except:
        print("Already exists")
    for subdirectory in subdirectories:
        filelist = list(map(lambda name: name, os.listdir('data/'+ directory + "/" + subdirectory)))
        try:
            os.mkdir('dataPreprocessed/' + directory + "/" + subdirectory)
        except:
            print("Already exists")     
        for filename in filelist:
            if filename is not ".DS_Store":
                image = io.imread('data/'+ directory + "/" + subdirectory + "/" + filename)
                img_adapteq = exposure.equalize_adapthist(image, clip_limit=0.03)
                io.imsave('dataPreprocessed/{}/{}/{}'.format(directory, subdirectory, filename), img_adapteq)
