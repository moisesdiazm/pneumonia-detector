document.addEventListener("DOMContentLoaded", function() {
  var spinner = document.getElementById("spinner-component");
  spinner.style.display = 'none';
});

function populateTable(table, rows, cells, content) {
  // if (!table) {
  //   table = document.createElement('table');
  //   table.classList.add("table");
  //   table.classList.add("table-striped");
  // }
  for (var i = 0; i < rows; ++i) {
      var row = document.createElement("tr");
      for (var j = 0; j < cells; ++j) {
          row.appendChild(document.createElement('td'));
          row.cells[j].appendChild(document.createTextNode("TEXTO"));
      }
      table.appendChild(row);
  }
  return table;
}

progressHandling = function (event) {
  var percent = 0;
  var position = event.loaded || event.position;
  var total = event.total;
  if (event.lengthComputable) {
      percent = Math.ceil(position / total * 100);
  }
  // update progressbars classes so it fits your code
  var temp = document.querySelector("#progress-bar");
  temp.value = percent;
};

function FileUpload() {

  var temp = document.querySelector("#fileUploadComponent");
  var button = document.getElementById("analyze-btn");
  var spinner = document.getElementById("spinner-component");
  button.style.display = 'none';
  spinner.style.display = 'block';

  console.log(temp.files);

  var data   = new FormData();
  
  temp.files.forEach(function(image, i) {
    data.append(image.name, image);
  });
  $.ajax({
    type: "POST",
    url: "http://localhost:5000/analyze",
    xhr: function () {
        var myXhr = $.ajaxSettings.xhr();
        if (myXhr.upload) {
            myXhr.upload.addEventListener('progress', progressHandling, false);
        }
        return myXhr;
    },
    success: function (data) {
      console.log(data);
      console.log(data.results)
      table = document.getElementById('resultstable')
      document.getElementById('tablearea').appendChild(populateTable(table, 2, 2, "Text"));
    },
    error: function (error) {
        // handle error
    },
    async: true,
    data: data,
    dataType : 'json',
    cache: false,
    contentType: false,
    processData: false,
    timeout: 60000
  });
}

