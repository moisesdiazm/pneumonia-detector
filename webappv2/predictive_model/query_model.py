import json, os, io, imageio
from PIL import Image
import numpy as np

os.environ['KERAS_BACKEND'] = 'theano'

from keras.models import model_from_json
from keras.preprocessing.image import img_to_array
from keras.applications import imagenet_utils

class KerasModel():

    def __init__(self,modelfile,weightsfile):
        # load json and create model
        json_file = open(modelfile, 'r')
        loaded_model_json = json_file.read()
        json_file.close()
        self.loaded_model = model_from_json(loaded_model_json)
        self.loaded_model.load_weights(weightsfile)
        print("Loaded Pneumonia Prediction model from disk")

    @staticmethod
    def prepare_image(image, target):
        # if the image mode is not RGB, convert it
        if image.mode != "RGB":
            image = image.convert("RGB")
            # resize the input image and preprocess it
        image = image.resize(target)
        image = img_to_array(image)
        image = np.expand_dims(image, axis=0)
        image = imagenet_utils.preprocess_input(image)
        # return the processed image
        return image
    
    def predict_multiple(self, image_names):
        results = []
        for imagename in image_names:
            image = Image.open(imagename)
            image_loaded = KerasModel.prepare_image(image, target=(150,150))
            preds = self.loaded_model.predict(image_loaded)
            _, filename = os.path.split(imagename)
            results.append({'filename': filename, 'prediction': int(round(preds[0][0]))})
        return results