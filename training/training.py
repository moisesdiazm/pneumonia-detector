import os
from keras import models
from keras import layers
from keras.applications import VGG16
from keras.applications.inception_v3 import InceptionV3
from keras.preprocessing.image import ImageDataGenerator
from keras import optimizers

import matplotlib.pyplot as plt 

base_dir = 'data'

train_dir = os.path.join(base_dir, 'train')
test_dir = os.path.join(base_dir, 'test')
validation_dir = os.path.join(base_dir, 'test')

IMAGESIZE = 150 # 150
DENSESIZE = 500 # 256
# Create  generators
# Create data augmentation for training data to avoid overfitting
train_datagenerator = ImageDataGenerator(rescale = 1.0/255, rotation_range=40, width_shift_range=0.2, shear_range=0.2, zoom_range=0.2, horizontal_flip=True)
# Does not create data augmentation for validation
validation_datagenerator = ImageDataGenerator(1.0/255)

# Generators application
train_generator = train_datagenerator.flow_from_directory(train_dir,  target_size=(IMAGESIZE,IMAGESIZE), batch_size=20, class_mode='binary')
validation_generator = validation_datagenerator.flow_from_directory(validation_dir, target_size=(IMAGESIZE,IMAGESIZE), batch_size=20, class_mode='binary')

# VGG or InceptionV3 model loading

conv_base = VGG16(weights='imagenet', include_top=False, input_shape=(150, 150, 3)) # Accepts black and white images
# conv_base = InceptionV3(include_top=False, weights='imagenet', input_tensor=None, input_shape=(IMAGESIZE,IMAGESIZE,3))

# Custom final layers model appending
model = models.Sequential()
model.add(conv_base)
model.add(layers.Flatten())
model.add(layers.Dense(500, activation='relu'))
model.add(layers.Dense(1, activation = 'sigmoid'))

model.summary() #Print summary representation

conv_base.trainable = False #Freeze convolutional base (VGG16)

model.compile(loss='binary_crossentropy', optimizer='rmsprop', metrics=['accuracy'])

training_process = model.fit_generator(train_generator, steps_per_epoch=100, epochs=200, validation_data=validation_generator, validation_steps=100) 

# serialize model to JSON
model_json = model.to_json()
with open("model_VGG16.json", "w") as json_file:
    json_file.write(model_json)
# serialize weights to HDF5
model.save_weights("model_VGG16.h5")
print("Saved model to disk")

accuracy = training_process.history['acc']
val_accuracy = training_process.history['val_acc']

epochs = range(1, len(accuracy) + 1)

plt.plot(epochs, accuracy, 'bo', label="Train accuracy")
plt.plot(epochs, val_accuracy, 'b', label="Validation accuracy")
plt.show()