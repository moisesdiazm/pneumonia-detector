import json, os, io
from flask import Flask, render_template, request, redirect, url_for, jsonify
from predictive_model.query_model import KerasModel
# import json, os, io, imageio
# from PIL import Image
# import numpy as np

# os.environ['KERAS_BACKEND'] = 'theano'


# from keras.models import model_from_json
# from keras.preprocessing.image import img_to_array
# from keras.applications import imagenet_utils

app = Flask(__name__)
app.config['UPLOAD_PATH'] = os.getcwd() + "/temporal"
pneumoniaPredictor = KerasModel(os.getcwd() + "/predictive_model/model.json", os.getcwd() + "/predictive_model/model.h5")
# MODEL LOADING
# model = None
# def load_model(modelfile,weightsfile):
#     global model
#     # load json and create model
#     json_file = open(modelfile, 'r')
#     loaded_model_json = json_file.read()
#     json_file.close()
#     model = model_from_json(loaded_model_json)
#     model.load_weights(weightsfile)
#     print("Loaded Pneumonia Prediction model from disk")


# def prepare_image(image, target):
#     # if the image mode is not RGB, convert it
#     if image.mode != "RGB":
#         image = image.convert("RGB")
#         # resize the input image and preprocess it
#     image = image.resize(target)
#     image = img_to_array(image)
#     image = np.expand_dims(image, axis=0)
#     image = imagenet_utils.preprocess_input(image)
#     # return the processed image
#     return image

# def predict_multiple(image_names):
#     results = []
#     for imagename in image_names:
#         image = Image.open(imagename)
#         image_loaded = prepare_image(image, target=(150,150))
#         preds = model.predict(image_loaded)
#         results.append({imagename: preds[0][0]})
#     return results

@app.route('/')
def home():
    return render_template("index.html")


@app.route('/analyze', methods=['POST'])
def analyze():
    print(request.files)
    images = []
    for filename,f in request.files.items():
        image_path = os.path.join(app.config['UPLOAD_PATH'], filename)
        print("SAVING")
        f.save(image_path)
        images.append(image_path)
    
    result = pneumoniaPredictor.predict_multiple(images)
    
    for filename,f in request.files.items():
        image_path = os.path.join(app.config['UPLOAD_PATH'], filename)
        os.remove(image_path)
        print("DELETING")
    
    print(type(result))
    print(list(result))
    return jsonify({'results': list(result)})


if __name__ == "__main__":
    app.run(debug=True)